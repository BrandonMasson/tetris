#include "piece.h"

#define CHAR_FIN "F"
#define CHAR_PIECE "#"
#define CHAR_RETOUR "R"
#define CHAR_VIDE "-"

#define CHAR_GRILLE "G"
#define CHAR_GRILLE_RETOUR "B"
#define CHAR_GRILLE_FIN "E"

#define NOM_FICHIER "parametre.txt"

//Compte le nombre de piece en ce servant du caractere de fin défini
int nbPiece();

//Remplit et retourne un tableau de piece
Piece *enregistrementPiece();

int hauteurGrille();

int largeurGrille();