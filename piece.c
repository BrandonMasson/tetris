#include "piece.h"
#include "graphique.h"

Piece *initialisationPiece()
{
    Piece *liste = malloc(sizeof(*liste));

    if (liste != NULL)
    {
        liste->nbElement = 0;
        return liste;
    }
    
    return NULL;
}

void insertionPiece(Piece *liste, int nvNombre)
{
    /* Création du nouvel élément */
    Element *nouveau = malloc(sizeof(Element));
    if (liste == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    
    
    nouveau->nombre = nvNombre;
    nouveau->suivant = NULL;
    
    if (liste->nbElement == 0)
    {
      liste->premier = nouveau;
    }
    else
    {
      Element* p = liste->premier;
      while(p->suivant != NULL)
      {
	p = p->suivant;
      }
      
      p->suivant = nouveau;
    }
    
    liste->nbElement += 1;
}

int largeurPiece(int numPiece, Piece *tabPiece)
{
  int compt = 0;
  int max = 0;
  
  if (tabPiece != NULL)
  {
    Piece *liste = NULL;
    Element *actuel = NULL;
      
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
      
    while (actuel != NULL)
    {
      if(actuel->nombre == 2)
      {
	if(max <= compt)
	{
	  max = compt;
	}
	compt = 0;
      }
      else
      {
	compt += 1;
      }
      
      actuel = actuel->suivant;  
    }
    
    if(max <= compt)
    {
      max = compt;
    }
    
    liste = NULL;
    actuel = NULL;
  }
  
  return max;
}

int hauteurPiece(int numPiece, Piece *tabPiece)
{
  int compt = 0;
  
  if (tabPiece != NULL)
  {
    Piece *liste = NULL;
    Element *actuel = NULL;
      
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
      
    while (actuel != NULL)
      {
	if(actuel->nombre == 2)
	{
	  compt += 1;
	}
	actuel = actuel->suivant;
      }
      liste = NULL;
      actuel = NULL;
  }
  
  return compt + 1;
}

int *afficherPiece(Piece *tabPiece, int numPiece, int numCouleur, int largeurGrille, int hauteurGrille, Carre **Grille, int nbPiece, SDL_Surface *screen, int *perdu)
{
    int *tabint = (int*) malloc ( sizeof(int) * 7);
    if (tabPiece != NULL && apparition_possible(tabPiece, numPiece, largeurGrille, hauteurGrille, Grille) == 1)
    {
      Piece *liste = NULL;
      Element *actuel = NULL;
      int comptX = (largeurGrille - largeurPiece(numPiece, tabPiece))/2;
      int comptY = 0;
      tabint[0] = comptX; 
      tabint[1] = comptY;
      tabint[2] = numPiece;
      tabint[3] = 0;
      tabint[4] = rand() % nbPiece;
      tabint[5] = numCouleur;
      tabint[6] = rand() % NB_COULEUR;
      
      while(numCouleur == tabint[6])
      {
          tabint[6] = rand() % NB_COULEUR;
      }
      
      while(numPiece == tabint[4])
      {
          tabint[4] = rand() % nbPiece;
      }
      
      futurePiece(tabPiece, tabint[4], tabint[6], screen);
      
      liste = &tabPiece[numPiece];
      actuel = liste->premier;
      
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    Grille[comptY][comptX].estPlein = 1;
            Grille[comptY][comptX].numCouleur = numCouleur;
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = (largeurGrille - largeurPiece(numPiece, tabPiece))/2;
	    comptY += 1;
	    break;
	}
	
	actuel = actuel->suivant;
      }
      liste = NULL;
      actuel = NULL;
    }
    else
    {
      *perdu = 1;
      tabint[0] = 0; 
      tabint[1] = 0;
      tabint[2] = -1;
      tabint[3] = 0;
      tabint[4] = 0;
      tabint[5] = 0;
    }
    return tabint;
}

int apparition_possible(Piece *tabPiece, int numPiece, int largeurGrille, int hauteurGrille, Carre **Grille)
{
  int comptX = (largeurGrille - largeurPiece(numPiece, tabPiece))/2;
  int comptY = 0;
  int res = 1;
  
  Piece *liste = NULL;
  Element *actuel = NULL;
  
  liste = &tabPiece[numPiece];
  actuel = liste->premier;
  
  while(actuel != NULL && res == 1)
  {
    switch(actuel->nombre)
    {
      case 0 : 
	comptX += 1;
	break;
      case 1 :
	if(Grille[comptY][comptX].estPlein == 1)
	{
	  res = 0;
	}
	comptX += 1;
	break;
      case 2 :
	comptX = (largeurGrille - largeurPiece(numPiece, tabPiece)) / 2;
	comptY += 1;
	break;
    }
    actuel = actuel->suivant;
  }
  return res;
}

int *movePiece(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int x, int tab[])
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int numCouleur = tab[5];
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
    
    if(peut_bouger(tabPiece, largeurGrille, hauteurGrille, Grille, x, tab) == 1)
    {
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 0;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      
      actuel = liste->premier;
      tab[0] += x;
      comptX = tab[0];
      comptY = tab[1];
      
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 1;
                Grille[comptY][comptX].numCouleur = numCouleur;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 1;
                    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 1;
                      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 1;
                    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 1;
                      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 1;
                        Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 1;
                    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 1;
                      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      liste = NULL;
      actuel = NULL;
    }
    return tab;
}

int *movePiece_down(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[], int nbPiece, SDL_Surface *screen, int *nbLigne, int *numLevel, int *Score, int *perdu)
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int numCouleur = tab[5];
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;

    if(peut_descendre(tabPiece, largeurGrille, hauteurGrille, Grille, tab) == 1)
    {
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 0;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      
      actuel = liste->premier;
      tab[1] += 1 ;
      comptX = tab[0];
      comptY = tab[1];
      
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 1;
                Grille[comptY][comptX].numCouleur = numCouleur;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 1;
                    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 1;
                      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 1;
                    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 1;
                      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 1;
                        Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 1;
                    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 1;
                      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      liste = NULL;
      actuel = NULL;
      
      //tab[1] += 1;
    }
    else
    {
       int ligne = verifie(largeurGrille, hauteurGrille, Grille);
       *nbLigne += ligne;
       *Score += score(ligne);
       *numLevel = calculLevel(*nbLigne);
       tab = afficherPiece(tabPiece, tab[4], tab[6], largeurGrille, hauteurGrille, Grille, nbPiece, screen, perdu);
       
       if(*perdu == 1)
       {
	 printf("Game Over\n");
       }
    }
    return tab;
}

int *rotation(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[])
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int numCouleur = tab[5];
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;

    if(peutTourner(tabPiece, largeurGrille, hauteurGrille, Grille, tab) == 1)
    {
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 0;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 0;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 0;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 0;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 0;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      
      actuel = liste->premier;
      
      comptX = tab[0];
      comptY = tab[1];
      numRotation += 1;
      
      if(numRotation == 4)
      {
	numRotation = 0;
      }
      
      while (actuel != NULL)
      {
	switch(actuel->nombre)
	{
	  case 0 : 
	    comptX += 1;
	    break;
	  case 1 :
	    switch(numRotation)
	    {
	      case 0 :
		Grille[comptY][comptX].estPlein = 1;
                Grille[comptY][comptX].numCouleur = numCouleur;
		break;
	      case 1 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
		    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].estPlein = 1;
                    Grille[comptY + (comptX - tab[0])][comptX - (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
		      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].estPlein = 1;
                      Grille[comptY - (comptY - tab[1])][comptX - (comptY - tab[1])].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
			Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[comptY + (comptX - tab[0] - 1)][comptX - (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 2 :
		if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = comptY + (comptX - tab[0]);
                    tempX = comptX - (comptX - tab[0]);
                      
		    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].estPlein = 1;
                    Grille[tempY - (tempY - tab[1])][tempX - (tempY - tab[1])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempY = comptY - (comptY - tab[1]);
                      tempX = comptX - (comptY - tab[1]);
                        
		      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].estPlein = 1;
                      Grille[tempY - (tab[0] - tempX)][tempX + (tab[0] - tempX)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = comptY + (comptX - tab[0] - 1);
                        tempX = comptX - (comptX - tab[0] + 1);
                        
			Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].estPlein = 1;
                        Grille[tempY - (tempY - tab[1] + 1)][tempX - (tempY - tab[1] - 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	      case 3 :
                if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
		{
		  if(faitPartie(tabPiece, Grille, tab, comptX, comptY) == 1)
		  {
		    Grille[comptY][comptX].estPlein = 1;
                    Grille[comptY][comptX].numCouleur = numCouleur;
		  }
		}
		else
		{
		  if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
		  {
                    tempY = (comptY + (comptX - tab[0]));
                    tempY2 = tempY - (tempY - tab[1]);
                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                      
		    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].estPlein = 1;
                    Grille[tempY2 - (comptX - tab[0])][tempX + (comptX - tab[0])].numCouleur = numCouleur;
		  }
		  else 
		  {
		    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
		    {
                      tempX = comptX - (comptY - tab[1]);
                      tempX2 = tempX + (tab[0] - tempX);
                      tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                      
		      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].estPlein = 1;
                      Grille[tempY + (tab[1] - tempY)][tempX2 + (tab[1] - tempY)].numCouleur = numCouleur;
		    }
		    else
		    {
		      if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
		      {
                        tempY = (comptY + (comptX - tab[0] - 1));
                        tempY2 = tempY - (tempY - tab[1] + 1);
                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                        
			Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].estPlein = 1;
                        Grille[tempY2 - (comptX - tab[0] - 1)][tempX + (comptX - tab[0] + 1)].numCouleur = numCouleur;
		      }
		    }
		  }
		}
		break;
	    }
	    
	    tempX = 0;
            tempY = 0;
            tempX2 = 0;
            tempY2 = 0;
            
	    comptX += 1;
	    break;
	  case 2 :
	    comptX = tab[0];
	    comptY += 1 ;
	    break;
	}
	actuel = actuel->suivant;
      }
      liste = NULL;
      actuel = NULL;
      tab[3] = numRotation;
    }
    
    return tab;
}

//regarde si une ligne est pleine dans le tableaux
int verifie(int largeurGrille, int hauteurGrille, Carre **Grille)
{
  int compt = 0;
  int nbLigne = 0;
  
  for (int i = hauteurGrille - 1; i >= 0; i--)
  {
      for (int j = 0; j < largeurGrille; j++)
      {
	if(Grille[i][j].estPlein == 1)
	{
	  compt += 1;
	}
      }
      
      if(compt == largeurGrille)
      {
	nbLigne += 1;
	for (int j = 0; j < largeurGrille; j++)
	{
	  Grille[i][j].estPlein = 0;
	}
	
	for(int k = i - 1; k >= 0; k--)
	{
	  for(int l = 0; l < largeurGrille; l++)
	  {
	    Grille[k + 1][l].estPlein = Grille[k][l].estPlein;
	    Grille[k + 1][l].numCouleur = Grille[k][l].numCouleur;
	    Grille[k][l].estPlein = 0;
	  }
	}
	
	i = hauteurGrille;
      }
      compt = 0;
  }
  
  return nbLigne;
}

int peutTourner(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[])
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3] + 1;
    
    if(numRotation == 4)
    {
        numRotation = 0;
    }
    
    int largeurPieceRotationMax = 0;
    int largeurPieceRotationMin = 0;
    int hauteurPieceRotationMax = 0;
    int hauteurPieceRotationMin = 0;
    int res = 1;
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
    
    switch(numRotation)
    {
        case 0 :
            largeurPieceRotationMax = comptX + largeurPiece(numPiece, tabPiece);
            largeurPieceRotationMin = tab[0];
            hauteurPieceRotationMax = comptY + hauteurPiece(numPiece, tabPiece);
            hauteurPieceRotationMin = tab[1];
            break;
        case 1 :
            largeurPieceRotationMax = (comptX + 1);
            largeurPieceRotationMin = tab[0] - (hauteurPiece(numPiece, tabPiece) - 1);
            hauteurPieceRotationMax = comptY + largeurPiece(numPiece, tabPiece);
            hauteurPieceRotationMin = tab[1];
            break;
        case 2 :
            largeurPieceRotationMax = (comptX + 1);
            largeurPieceRotationMin = tab[0] - (largeurPiece(numPiece, tabPiece) - 1);
            hauteurPieceRotationMax = comptY + hauteurPiece(numPiece, tabPiece);
            hauteurPieceRotationMin = tab[1] - (hauteurPiece(numPiece, tabPiece) - 1);
            break;
        case 3 :
            largeurPieceRotationMax = comptX + hauteurPiece(numPiece, tabPiece);
            largeurPieceRotationMin = tab[0];
            hauteurPieceRotationMax = comptY;
            hauteurPieceRotationMin = tab[1] - (largeurPiece(numPiece, tabPiece) - 1);
            break;
    }
    
    if((largeurPieceRotationMax <= largeurGrille) && (largeurPieceRotationMin >= 0) && (hauteurPieceRotationMax <= hauteurGrille) && (hauteurPieceRotationMin >= 0))
    {
        while (actuel != NULL && res == 1)
        {
            switch(actuel->nombre)
            {
                case 0 : 
                    comptX += 1;
                    break;
                case 1 :
                    switch(numRotation)
                    {
                        case 0 :
                            if(Grille[comptY][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY) == 0)
                            {
                                res = 0;
                            }
                            break;
                        case 1 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    if(Grille[(comptY + (comptX - tab[0]))][(comptX - (comptX - tab[0]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0])), (comptY + (comptX - tab[0]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        if(Grille[(comptY - (comptY - tab[1]))][(comptX - (comptY - tab[1]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptY - tab[1])), (comptY - (comptY - tab[1]))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            if(Grille[(comptY + (comptX - tab[0] - 1))][(comptX - (comptX - tab[0] + 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0] + 1)), (comptY + (comptX - tab[0] - 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 2 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = comptY + (comptX - tab[0]);
                                    tempX = comptX - (comptX - tab[0]);
                                    
                                    if(Grille[(tempY - (tempY - tab[1]))][(tempX - (tempY - tab[1]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1])), (tempY - (tempY - tab[1]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempY = comptY - (comptY - tab[1]);
                                        tempX = comptX - (comptY - tab[1]);
                                        
                                        if(Grille[(tempY - (tab[0] - tempX))][(tempX + (tab[0] - tempX))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (tab[0] - tempX)), (tempY - (tab[0] - tempX))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = comptY + (comptX - tab[0] - 1);
                                            tempX = comptX - (comptX - tab[0] + 1);
                                            
                                            if(Grille[(tempY - (tempY - tab[1] + 1))][(tempX - (tempY - tab[1] - 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1] - 1)), (tempY - (tempY - tab[1] + 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 3 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = (comptY + (comptX - tab[0]));
                                    tempY2 = tempY - (tempY - tab[1]);
                                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                                    
                                    if(Grille[(tempY2 - (comptX - tab[0]))][(tempX + (comptX - tab[0]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0])), (tempY2 - (comptX - tab[0]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempX = comptX - (comptY - tab[1]);
                                        tempX2 = tempX + (tab[0] - tempX);
                                        tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                                        
                                        if(Grille[(tempY + (tab[1] - tempY))][(tempX2 + (tab[1] - tempY))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX2 + (tab[1] - tempY)), (tempY + (tab[1] - tempY))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = (comptY + (comptX - tab[0] - 1));
                                            tempY2 = tempY - (tempY - tab[1] + 1);
                                            tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                                            
                                            if(Grille[(tempY2 - (comptX - tab[0] - 1))][(tempX + (comptX - tab[0] + 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0] + 1)), (tempY2 - (comptX - tab[0] - 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                    
                    tempX = 0;
                    tempY = 0;
                    tempX2 = 0;
                    tempY2 = 0;
                    
                    comptX += 1;
                    break;
                case 2 :
                    comptX = tab[0];
                    comptY += 1 ;
                    break;
            }
            actuel = actuel->suivant;
        }
    }
    else
    {
      res = 0;
    }
    return res;
}

int peut_bouger(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int x, int tab[])
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int largeurPieceRotationMax = 0;
    int largeurPieceRotationMin = 0;
    int res = 1;
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
    
    switch(numRotation)
    {
        case 0 :
            largeurPieceRotationMax = comptX + largeurPiece(numPiece, tabPiece) + x;
            largeurPieceRotationMin = tab[0] + x;
            break;
        case 1 :
            largeurPieceRotationMax = (comptX + 1) + x;
            largeurPieceRotationMin = tab[0] - (hauteurPiece(numPiece, tabPiece) - 1) + x;
            break;
        case 2 :
            largeurPieceRotationMax = (comptX + 1) + x;
            largeurPieceRotationMin = tab[0] - (largeurPiece(numPiece, tabPiece) - 1) + x;
            break;
        case 3 :
            largeurPieceRotationMax = comptX + hauteurPiece(numPiece, tabPiece) + x;
            largeurPieceRotationMin = tab[0] + x;
            break;
    }
    
    if((largeurPieceRotationMax <= largeurGrille) && (largeurPieceRotationMin >= 0))
    {
        while (actuel != NULL && res == 1)
        {
            switch(actuel->nombre)
            {
                case 0 : 
                    comptX += 1;
                    break;
                case 1 :
                    switch(numRotation)
                    {
                        case 0 :
                            if(Grille[comptY][comptX + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX + x, comptY) == 0)
                            {
                                res = 0;
                            }
                            break;
                        case 1 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX + x, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    if(Grille[(comptY + (comptX - tab[0]))][(comptX - (comptX - tab[0])) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0])) + x, (comptY + (comptX - tab[0]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        if(Grille[(comptY - (comptY - tab[1]))][(comptX - (comptY - tab[1])) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptY - tab[1])) + x, (comptY - (comptY - tab[1]))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            if(Grille[(comptY + (comptX - tab[0] - 1))][(comptX - (comptX - tab[0] + 1)) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0] + 1)) + x, (comptY + (comptX - tab[0] - 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 2 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX + x, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = comptY + (comptX - tab[0]);
                                    tempX = comptX - (comptX - tab[0]);
                                    
                                    if(Grille[(tempY - (tempY - tab[1]))][(tempX - (tempY - tab[1])) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1])) + x, (tempY - (tempY - tab[1]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempY = comptY - (comptY - tab[1]);
                                        tempX = comptX - (comptY - tab[1]);
                                        
                                        if(Grille[(tempY - (tab[0] - tempX))][(tempX + (tab[0] - tempX)) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (tab[0] - tempX)) + x, (tempY - (tab[0] - tempX))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = comptY + (comptX - tab[0] - 1);
                                            tempX = comptX - (comptX - tab[0] + 1);
                                            
                                            if(Grille[(tempY - (tempY - tab[1] + 1))][(tempX - (tempY - tab[1] - 1)) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1] - 1)) + x, (tempY - (tempY - tab[1] + 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 3 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY][comptX + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX + x, comptY) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = (comptY + (comptX - tab[0]));
                                    tempY2 = tempY - (tempY - tab[1]);
                                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                                    
                                    if(Grille[(tempY2 - (comptX - tab[0]))][(tempX + (comptX - tab[0])) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0])) + x, (tempY2 - (comptX - tab[0]))) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempX = comptX - (comptY - tab[1]);
                                        tempX2 = tempX + (tab[0] - tempX);
                                        tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                                        
                                        if(Grille[(tempY + (tab[1] - tempY))][(tempX2 + (tab[1] - tempY)) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX2 + (tab[1] - tempY)) + x, (tempY + (tab[1] - tempY))) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = (comptY + (comptX - tab[0] - 1));
                                            tempY2 = tempY - (tempY - tab[1] + 1);
                                            tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                                            
                                            if(Grille[(tempY2 - (comptX - tab[0] - 1))][(tempX + (comptX - tab[0] + 1)) + x].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0] + 1)) + x, (tempY2 - (comptX - tab[0] - 1))) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                    
                    tempX = 0;
                    tempY = 0;
                    tempX2 = 0;
                    tempY2 = 0;
                    
                    comptX += 1;
                    break;
                case 2 :
                    comptX = tab[0];
                    comptY += 1 ;
                    break;
            }
            actuel = actuel->suivant;
        }
    }
    else
    {
      res = 0;
    }
    return res;
}

int peut_descendre(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[])
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int hauteurPieceRotation = 0;
    int res = 1;
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
    
    switch(numRotation)
    {
        case 0 :
            hauteurPieceRotation = comptY + hauteurPiece(numPiece, tabPiece) + 1;
            break;
        case 1 :
            hauteurPieceRotation = comptY + largeurPiece(numPiece, tabPiece) + 1;
            break;
        case 2 :
            hauteurPieceRotation = comptY + hauteurPiece(numPiece, tabPiece);
            break;
        case 3 :
            hauteurPieceRotation = comptY + 2;
            break;
    }

    if(hauteurPieceRotation <= hauteurGrille)
    {
        while (actuel != NULL && res == 1)
        {
            switch(actuel->nombre)
            {
                case 0 : 
                    comptX += 1;
                    break;
                case 1 :
                    switch(numRotation)
                    {
                        case 0 :
                            if(Grille[comptY + 1][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY + 1) == 0)
                            {
                                res = 0;
                            }
                            break;
                        case 1 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY + 1][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY + 1) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    if(Grille[(comptY + (comptX - tab[0])) + 1][(comptX - (comptX - tab[0]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0])), (comptY + (comptX - tab[0])) + 1) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        if(Grille[(comptY - (comptY - tab[1])) + 1][(comptX - (comptY - tab[1]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptY - tab[1])), (comptY - (comptY - tab[1])) + 1) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            if(Grille[(comptY + (comptX - tab[0] - 1)) + 1][(comptX - (comptX - tab[0] + 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (comptX - (comptX - tab[0] + 1)), (comptY + (comptX - tab[0] - 1)) + 1) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 2 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY + 1][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY + 1) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = comptY + (comptX - tab[0]);
                                    tempX = comptX - (comptX - tab[0]);
                                    
                                    if(Grille[(tempY - (tempY - tab[1])) + 1][(tempX - (tempY - tab[1]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1])), (tempY - (tempY - tab[1])) + 1) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempY = comptY - (comptY - tab[1]);
                                        tempX = comptX - (comptY - tab[1]);
                                        
                                        if(Grille[(tempY - (tab[0] - tempX)) + 1][(tempX + (tab[0] - tempX))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (tab[0] - tempX)), (tempY - (tab[0] - tempX)) + 1) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = comptY + (comptX - tab[0] - 1);
                                            tempX = comptX - (comptX - tab[0] + 1);
                                            
                                            if(Grille[(tempY - (tempY - tab[1] + 1)) + 1][(tempX - (tempY - tab[1] - 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX - (tempY - tab[1] - 1)), (tempY - (tempY - tab[1] + 1)) + 1) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 3 :
                            if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                            {
                                if(Grille[comptY + 1][comptX].estPlein == 1 && faitPartie(tabPiece, Grille, tab, comptX, comptY + 1) == 0)
                                {
                                    res = 0;
                                }
                            }
                            else
                            {
                                if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                                {
                                    tempY = (comptY + (comptX - tab[0]));
                                    tempY2 = tempY - (tempY - tab[1]);
                                    tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                                    
                                    if(Grille[(tempY2 - (comptX - tab[0])) + 1][(tempX + (comptX - tab[0]))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0])), (tempY2 - (comptX - tab[0])) + 1) == 0)
                                    {
                                        res = 0;
                                    }
                                }
                                else 
                                {
                                    if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempX = comptX - (comptY - tab[1]);
                                        tempX2 = tempX + (tab[0] - tempX);
                                        tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                                        
                                        if(Grille[(tempY + (tab[1] - tempY)) + 1][(tempX2 + (tab[1] - tempY))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX2 + (tab[1] - tempY)), (tempY + (tab[1] - tempY)) + 1) == 0)
                                        {
                                            res = 0;
                                        }
                                    }
                                    else
                                    {
                                        if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                        {
                                            tempY = (comptY + (comptX - tab[0] - 1));
                                            tempY2 = tempY - (tempY - tab[1] + 1);
                                            tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                                            
                                            if(Grille[(tempY2 - (comptX - tab[0] - 1)) + 1][(tempX + (comptX - tab[0] + 1))].estPlein == 1 && faitPartie(tabPiece, Grille, tab, (tempX + (comptX - tab[0] + 1)), (tempY2 - (comptX - tab[0] - 1)) + 1) == 0)
                                            {
                                                res = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                    
                    tempX = 0;
                    tempY = 0;
                    tempX2 = 0;
                    tempY2 = 0;
                    
                    comptX += 1;
                    break;
                case 2 :
                    comptX = tab[0];
                    comptY += 1 ;
                    break;
            }
            actuel = actuel->suivant;
        }
    }
    else
    {
      res = 0;
    }
    return res;
}

int faitPartie(Piece *tabPiece, Carre **Grille, int tab[], int x, int y)
{
    int comptX = tab[0];
    int comptY = tab[1];
    int numPiece = tab[2];
    int numRotation = tab[3];
    int res = 0;
    int tempX, tempY, tempY2, tempX2;
    
    tempX = 0;
    tempY = 0;
    tempX2 = 0;
    tempY2 = 0;
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
    
    while (actuel != NULL && res == 0)
    {
        switch(actuel->nombre)
	{
            case 0 : 
                comptX += 1;
                break;
            case 1 :
                switch(numRotation)
                {
                    case 0 :
                        if(comptX == x && comptY == y)
                        {
                            res = 1;
                        }
                        break;
                    case 1 :
                        if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                        {
                            if(comptX == x && comptY == y)
                            {
                                res = 1;
                            }
                        }
                        else
                        {
                            if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                            {
                                if((comptX - (comptX - tab[0])) == x && (comptY + (comptX - tab[0])) == y)
                                {
                                    res = 1;
                                }
                            }
                            else 
                            {
                                if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                {
                                    if((comptX - (comptY - tab[1])) == x && (comptY - (comptY - tab[1])) == y)
                                    {
                                        res = 1;
                                    }
                                }
                                else
                                {
                                    if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                    {
                                        if((comptX - (comptX - tab[0] + 1)) == x && (comptY + (comptX - tab[0] - 1)) == y)
                                        {
                                            res = 1;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 2 :
                        if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                        {
                            if(comptX == x && comptY == y)
                            {
                                res = 1;
                            }
                        }
                        else
                        {
                            if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                            {
                                tempY = comptY + (comptX - tab[0]);
                                tempX = comptX - (comptX - tab[0]);
                                
                                if((tempX - (tempY - tab[1])) == x && (tempY - (tempY - tab[1])) == y)
                                {
                                    res = 1;
                                }
                            }
                            else 
                            {
                                if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                {
                                    tempY = comptY - (comptY - tab[1]);
                                    tempX = comptX - (comptY - tab[1]);
                                    
                                    if((tempX + (tab[0] - tempX)) == x && (tempY - (tab[0] - tempX)) == y)
                                    {
                                        res = 1;
                                    }
                                }
                                else
                                {
                                    if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempY = comptY + (comptX - tab[0] - 1);
                                        tempX = comptX - (comptX - tab[0] + 1);
                                        
                                        if((tempX - (tempY - tab[1] - 1)) == x && (tempY - (tempY - tab[1] + 1)) == y)
                                        {
                                            res = 1;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 3 :
                        if((comptX - tab[0]) == 0 && (comptY - tab[1]) == 0)
                        {
                            if(comptX == x && comptY == y)
                            {
                                res = 1;
                            }
                        }
                        else
                        {
                            if((comptX - tab[0]) > 0 && (comptY - tab[1]) == 0)
                            {
                                tempY = (comptY + (comptX - tab[0]));
                                tempY2 = tempY - (tempY - tab[1]);
                                tempX = (comptX - (comptX - tab[0])) - (tempY - tab[1]);
                                
                                if((tempX + (comptX - tab[0])) == x && (tempY2 - (comptX - tab[0])) == y)
                                {
                                    res = 1;
                                }
                            }
                            else 
                            {
                                if((comptX - tab[0]) == 0 && (comptY - tab[1]) > 0)
                                {
                                    tempX = comptX - (comptY - tab[1]);
                                    tempX2 = tempX + (tab[0] - tempX);
                                    tempY = (comptY - (comptY - tab[1])) - (tab[0] - tempX);
                                    
                                    if((tempX2 + (tab[1] - tempY)) == x && (tempY + (tab[1] - tempY)) == y)
                                    {
                                        res = 1;
                                    }
                                }
                                else
                                {
                                    if((comptX - tab[0]) > 0 && (comptY - tab[1]) > 0)
                                    {
                                        tempY = (comptY + (comptX - tab[0] - 1));
                                        tempY2 = tempY - (tempY - tab[1] + 1);
                                        tempX = (comptX - (comptX - tab[0] + 1)) - (tempY - tab[1] - 1);
                                        
                                        if((tempX + (comptX - tab[0] + 1)) == x && (tempY2 - (comptX - tab[0] - 1)) == y)
                                        {
                                            res = 1;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }
                
                tempX = 0;
                tempY = 0;
                tempX2 = 0;
                tempY2 = 0;
                
                comptX += 1;
                break;
            case 2 :
                comptX = tab[0];
                comptY += 1 ;
                break;
        }
        actuel = actuel->suivant;
    }
    
    return res;
}

Carre **initialisationGrille(int hauteurGrille, int largeurGrille)
{
  Carre **Grille = (Carre**)malloc(hauteurGrille * sizeof(Carre*));
  
  for (int i = 0; i < hauteurGrille; i++)
  {
    Grille[i] = (Carre*)malloc(largeurGrille * sizeof(Carre));
  } 
  
  for (int i = 0; i < hauteurGrille; i++)
  {
    for (int j = 0; j < largeurGrille; j++)
    {
      Grille[i][j].estPlein = 0;
      Grille[i][j].numCouleur = 0;
    }
  } 
  
  return Grille;
}

void futurePiece(Piece *tabPiece, int numPiece, int numCouleur, SDL_Surface *screen)
{
    int sprite_size = realSpriteSize();
    int x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + (((10 - largeurPiece(numPiece, tabPiece)) / 2) * sprite_size);
    int y = 10 + (((5 - hauteurPiece(numPiece, tabPiece)) / 2) * sprite_size);
    
    Piece *liste = NULL;
    Element *actuel = NULL;
    
    liste = &tabPiece[numPiece];
    actuel = liste->premier;
      
    while (actuel != NULL)
    {
        switch(actuel->nombre)
	{
	  case 0 : 
	    x += sprite_size;
	    break;
	  case 1 :
	    afficherCarre(x, y, screen, createSprite(couleurCarre(numCouleur)));
	    x += sprite_size;
	    break;
	  case 2 :
	    x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + (((10 - largeurPiece(numPiece, tabPiece)) / 2) * sprite_size);
	    y += sprite_size;
	    break;
	}
	actuel = actuel->suivant;
    }
    liste = NULL;
    actuel = NULL;
}

void actu(SDL_Surface *screen, int largeur_Grille, int hauteur_Grille, Carre **Grille, int jeuEnCours, int jeuEnPause, int hoverQuit, int hoverPause, int hoverPlay, int nbLigne, int numLevel, int Score, int perdu)
{
    int sprite_size = realSpriteSize();
    
    screen = drawBackground(screen);
    
    SDL_Rect gridImage;
    gridImage.x = 0;
    gridImage.y = 0;
    gridImage.w = sprite_size;
    gridImage.h = sprite_size;  
    
    SDL_Rect bordH;
    bordH.x = 0;
    bordH.y = 0;
    bordH.w = sprite_size;
    bordH.h = BORD_SIZE;  
    
    SDL_Rect bordV;
    bordV.x = 0;
    bordV.y = 0;
    bordV.w = BORD_SIZE;
    bordV.h = sprite_size;  
    
    SDL_Rect coin;
    coin.x = 0;
    coin.y = 0;
    coin.w = BORD_SIZE;
    coin.h = BORD_SIZE; 
  
    for (int y = 0; y < hauteur_Grille; y++)
    {
        gridImage.y = (y * sprite_size) + ((SCREEN_HEIGHT / 2) - ((sprite_size * hauteur_Grille) / 2));
        
        for (int x = 0; x < largeur_Grille; x++) 
        {
            gridImage.x = (x * sprite_size) + ((SCREEN_WIDTH / 4) - ((sprite_size * largeur_Grille) / 2));
            
            SDL_BlitSurface(createSprite(FICHIER_GRILLE_MILIEU), NULL, screen, &gridImage);
            
            if(y == 0 && x == 0)
            {
                bordV.y = gridImage.y;
                bordV.x = gridImage.x - BORD_SIZE;
                
                bordH.y = gridImage.y - BORD_SIZE;
                bordH.x = gridImage.x;
                
                coin.y = gridImage.y - BORD_SIZE;
                coin.x = gridImage.x - BORD_SIZE;
                
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HAUT_GAUCHE, coin.w, coin.h), NULL, screen, &coin);
            }
            else
            {
                if(y == 0 && x == (largeur_Grille - 1))
                {
                    bordV.y = gridImage.y;
                    bordV.x = gridImage.x + sprite_size;
                    
                    bordH.y = gridImage.y - BORD_SIZE;
                    bordH.x = gridImage.x;
                    
                    coin.y = gridImage.y - BORD_SIZE;
                    coin.x = gridImage.x + sprite_size;
                    
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HAUT_DROIT, coin.w, coin.h), NULL, screen, &coin);
                }
                else
                {
                    if(y == 0 && x > 0 && x < largeur_Grille)
                    {
                    
                        bordH.y = gridImage.y - BORD_SIZE;
                        bordH.x = gridImage.x;
                    
                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                    }
                    else
                    {
                        if(y > 0 && y < (hauteur_Grille - 1) && x == 0)
                        {
                            bordV.y = gridImage.y;
                            bordV.x = gridImage.x - BORD_SIZE;
                    
                            SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);                    
                        }
                        else
                        {
                            if(y > 0 && y < (hauteur_Grille - 1) && x == (largeur_Grille - 1))
                            {
                                bordV.y = gridImage.y;
                                bordV.x = gridImage.x + sprite_size;
                        
                                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);     
                            }
                            else
                            {
                                if(y == (hauteur_Grille - 1) && x == 0)
                                {
                                    bordV.y = gridImage.y;
                                    bordV.x = gridImage.x - BORD_SIZE;
                                    
                                    bordH.y = gridImage.y + sprite_size;
                                    bordH.x = gridImage.x;
                                    
                                    coin.y = gridImage.y + sprite_size;
                                    coin.x = gridImage.x - BORD_SIZE;
                                    
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_BAS_GAUCHE, coin.w, coin.h), NULL, screen, &coin);
                                }
                                else
                                {
                                    if(y == (hauteur_Grille - 1) && x == (largeur_Grille - 1))
                                    {
                                        bordV.y = gridImage.y;
                                        bordV.x = gridImage.x + sprite_size;
                                        
                                        bordH.y = gridImage.y + sprite_size;
                                        bordH.x = gridImage.x;
                                        
                                        coin.y = gridImage.y + sprite_size;
                                        coin.x = gridImage.x + sprite_size;
                                        
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_BAS_DROIT, coin.w, coin.h), NULL, screen, &coin);
                                    }
                                    else
                                    {
                                        if(y == (hauteur_Grille - 1) && x > 0 && x < largeur_Grille)
                                        {
                                            bordH.y = gridImage.y + sprite_size;
                                            bordH.x = gridImage.x;
                                        
                                            SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    gridImage.x = 0;
    gridImage.y = 0;
    
    bordH.x = 0;
    bordH.y = 0;
    
    bordV.x = 0;
    bordV.y = 0; 
    
    coin.x = 0;
    coin.y = 0;
    
    for (int y = 0; y < 5; y++)
    {
        gridImage.y = (y * sprite_size) + 10;
        
        for (int x = 0; x < 10; x++) 
        {
            gridImage.x = (x * sprite_size) + ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18);
            
            SDL_BlitSurface(createSprite(FICHIER_FOND), NULL, screen, &gridImage);
            
            if(y == 0 && x == 0)
            {
                bordV.y = gridImage.y;
                bordV.x = gridImage.x - BORD_SIZE;
                
                bordH.y = gridImage.y - BORD_SIZE;
                bordH.x = gridImage.x;
                
                coin.y = gridImage.y - BORD_SIZE;
                coin.x = gridImage.x - BORD_SIZE;
                
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HAUT_GAUCHE, coin.w, coin.h), NULL, screen, &coin);
            }
            else
            {
                if(y == 0 && x == 9)
                {
                    bordV.y = gridImage.y;
                    bordV.x = gridImage.x + sprite_size;
                    
                    bordH.y = gridImage.y - BORD_SIZE;
                    bordH.x = gridImage.x;
                    
                    coin.y = gridImage.y - BORD_SIZE;
                    coin.x = gridImage.x + sprite_size;
                    
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HAUT_DROIT, coin.w, coin.h), NULL, screen, &coin);
                }
                else
                {
                    if(y == 0 && x > 0 && x < 10)
                    {
                    
                        bordH.y = gridImage.y - BORD_SIZE;
                        bordH.x = gridImage.x;
                    
                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                    }
                    else
                    {
                        if(y > 0 && y < 4 && x == 0)
                        {
                            bordV.y = gridImage.y;
                            bordV.x = gridImage.x - BORD_SIZE;
                    
                            SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);                    
                        }
                        else
                        {
                            if(y > 0 && y < 4 && x == 9)
                            {
                                bordV.y = gridImage.y;
                                bordV.x = gridImage.x + sprite_size;
                        
                                SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);     
                            }
                            else
                            {
                                if(y == 4 && x == 0)
                                {
                                    bordV.y = gridImage.y;
                                    bordV.x = gridImage.x - BORD_SIZE;
                                    
                                    bordH.y = gridImage.y + sprite_size;
                                    bordH.x = gridImage.x;
                                    
                                    coin.y = gridImage.y + sprite_size;
                                    coin.x = gridImage.x - BORD_SIZE;
                                    
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                    SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_BAS_GAUCHE, coin.w, coin.h), NULL, screen, &coin);
                                }
                                else
                                {
                                    if(y == 4 && x == 9)
                                    {
                                        bordV.y = gridImage.y;
                                        bordV.x = gridImage.x + sprite_size;
                                        
                                        bordH.y = gridImage.y + sprite_size;
                                        bordH.x = gridImage.x;
                                        
                                        coin.y = gridImage.y + sprite_size;
                                        coin.x = gridImage.x + sprite_size;
                                        
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_VERTICAL, bordV.w, bordV.h), NULL, screen, &bordV);
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                        SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_BAS_DROIT, coin.w, coin.h), NULL, screen, &coin);
                                    }
                                    else
                                    {
                                        if(y == 4 && x > 0 && x < 10)
                                        {
                                            bordH.y = gridImage.y + sprite_size;
                                            bordH.x = gridImage.x;
                                        
                                            SDL_BlitSurface(createSpriteWH(FICHIER_GRILLE_HORIZONTAL, bordH.w, bordH.h), NULL, screen, &bordH);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18);
    gridImage.y = ((4 * sprite_size) + 10) + 50;
    gridImage.w = 53;
    gridImage.h = 28;
    
    SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_LEVEL, gridImage.w, gridImage.h), NULL, screen, &gridImage);
    
    gridImage.x = SCREEN_WIDTH - 25;
    gridImage.y = ((4 * sprite_size) + 10) + 50;
    
    char str[20];
    sprintf(str, "%d", numLevel);
    
    BlitIntSprite(str, screen, gridImage);
    
    gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18);
    gridImage.y = ((4 * sprite_size) + 10) + 100;
    gridImage.w = 53;
    gridImage.h = 29;
    
    SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_LINES, gridImage.w, gridImage.h), NULL, screen, &gridImage);
    
    gridImage.x = SCREEN_WIDTH - 25;
    gridImage.y = ((4 * sprite_size) + 10) + 100;
    
    sprintf(str, "%d", nbLigne);
    
    BlitIntSprite(str, screen, gridImage);
    
    gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18);
    gridImage.y = ((4 * sprite_size) + 10) + 150;
    gridImage.w = 64;
    gridImage.h = 25;
    
    SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_SCORE, gridImage.w, gridImage.h), NULL, screen, &gridImage);
    
    gridImage.x = SCREEN_WIDTH - 25;
    gridImage.y = ((4 * sprite_size) + 10) + 150;
    
    sprintf(str, "%d", Score);
    
    BlitIntSprite(str, screen, gridImage);
    
    if((jeuEnCours == 0 || jeuEnPause == 1) && perdu == 0)
    {
        gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 75;
        gridImage.y = ((4 * sprite_size) + 10) + 200;
        gridImage.w = 49;
        gridImage.h = 30;
        
        SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_PLAY, gridImage.w, gridImage.h), NULL, screen, &gridImage);
    }
    else
    {
        gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18);
        gridImage.y = ((4 * sprite_size) + 10) + 200;
        gridImage.w = 44;
        gridImage.h = 27;
        
        if(hoverQuit == 0)
        {
            SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_QUIT, gridImage.w, gridImage.h), NULL, screen, &gridImage);
        }
        else
        {
            SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_QUIT_HOVER, gridImage.w, gridImage.h), NULL, screen, &gridImage);
        }
        
        gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 150;
        gridImage.y = ((4 * sprite_size) + 10) + 200;
        gridImage.w = 61;
        gridImage.h = 24;
        
        if(perdu == 0)
        {
            if(hoverPause == 0)
            {
                SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_PAUSE, gridImage.w, gridImage.h), NULL, screen, &gridImage);
            }
            else
            {
                SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_PAUSE_HOVER, gridImage.w, gridImage.h), NULL, screen, &gridImage);
            }
        }
    }
    
    gridImage.x = ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 30);
    gridImage.y = SCREEN_HEIGHT - 90;
    gridImage.w = 198;
    gridImage.h = 81;
    
    SDL_BlitSurface(createSpriteWH(FICHIER_LABEL_TETRIS, gridImage.w, gridImage.h), NULL, screen, &gridImage);
  
    for(int i = 0; i < hauteur_Grille; i++)
    {
        for(int j = 0; j < largeur_Grille; j++)
        {
            if(Grille[i][j].estPlein == 1)
            {
                afficherCarre((j * sprite_size) + ((SCREEN_WIDTH / 4) - ((sprite_size * largeur_Grille) / 2)), (i * sprite_size) + ((SCREEN_HEIGHT / 2) - ((sprite_size * hauteur_Grille) / 2)), screen, createSprite(couleurCarre(Grille[i][j].numCouleur)));
            }
        }
    }
}
  
int score(int nbligne)
{
    if(nbligne == 0)
    {
        return 0;
    }
    else
    {
        if(nbligne == 1)
        {
            return POINT;
        }
        else
        {
            if(nbligne == 2)
            {
                return (POINT * 2) + POINT;
            }
            else
            {
                if(nbligne == 3)
                {
                    return (POINT * 3) + (POINT * 2);
                }
                else
                {
                    return (POINT * nbligne) + (POINT * 3);
                }
            }
        }
    }
}

int calculLevel(int nbligne)
{
        if(nbligne >= 90)
        {
            return 10;
        }
        else
        {
            if(nbligne >= 80)
            {
                return 9;
            }
            else
            {
                if(nbligne >= 70)
                {
                    return 8;
                }
                else
                {
                    if(nbligne >= 60)
                    {
                        return 7;
                    }
                    else
                    {
                        if(nbligne >= 50)
                        {
                            return 6;
                        }
                        else
                        {
                            if(nbligne >= 40)
                            {
                                return 5;
                            }
                            else
                            {
                                if(nbligne >= 30)
                                {
                                    return 4;
                                }
                                else
                                {
                                    if(nbligne >= 20)
                                    {
                                        return 3;
                                    }
                                    else
                                    {
                                        if(nbligne >= 10)
                                        {
                                            return 2;
                                        }
                                        else
                                        {
                                            return 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
}