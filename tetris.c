#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <time.h>
#include "fichier.h"
#include "graphique.h"

/* Order of the different directions in the picture: */
#define DIR_UP          0
#define DIR_RIGHT       1
#define DIR_DOWN        2
#define DIR_LEFT        3

int *HandleEvent(SDL_Event event, int *quit, Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tabPosition[], SDL_Surface *screen, int *jeuEnCours, int *jeuEnPause, int *hoverQuit, int *hoverPause, int *hoverPlay, int *nbLigne, int *numLevel, int *score, int *perdu)
{
    int sprite_size = realSpriteSize();
    switch (event.type) 
    {
        //close button clicked
        case SDL_QUIT :
            *quit = 1;
            break;
            
        case SDL_MOUSEMOTION :
            if(event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) && event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 44 && event.motion.y >= ((4 * sprite_size) + 10) + 200 && event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 27)
            {
                *hoverQuit = 1;
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            else
            {
                *hoverQuit = 0;
            }
            
            if(event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 150 && event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 150 + 61 && event.motion.y >= ((4 * sprite_size) + 10) + 200 && event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 24)
            {
                *hoverPause = 1;
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            else
            {
                *hoverPause = 0;
            }
            
            if(event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 75 && event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 49 && event.motion.y >= ((4 * sprite_size) + 10) + 200 && event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 30)
            {
                *hoverPlay = 1;
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            else
            {
                *hoverPlay = 0;
            }
            break;
            
        case SDL_MOUSEBUTTONUP:
            if(event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) && event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 44 && event.motion.y >= ((4 * sprite_size) + 10) + 200 && event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 27)
            {
                *quit = 1;
                
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            
            if(event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 150 && event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 150 + 61 && event.motion.y >= ((4 * sprite_size) + 10) + 200 && event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 24)
            {
                *jeuEnPause = 1;
                
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            
            if(((event.motion.x >= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 75) && (event.motion.x <= ((SCREEN_WIDTH / 4) + ((sprite_size * 10) / 2) + 18) + 75 + 49)) && ((event.motion.y >= ((4 * sprite_size) + 10) + 200) && (event.motion.y <=  ((4 * sprite_size) + 10) + 200 + 30)))
            {
                if(*jeuEnCours == 0)
                {
                    *jeuEnCours = 1;
                }
                
                if(*jeuEnPause == 1)
                {
                    *jeuEnPause = 0;
                }
                
                actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
            break;

        //handle the keyboard
        case SDL_KEYDOWN :
            switch (event.key.keysym.sym) 
	    {
                case SDLK_ESCAPE:
                case SDLK_q:
                    *quit = 1;
                    break;
                case SDLK_LEFT:
		    if(*perdu == 0 && *jeuEnCours == 1 && *jeuEnPause == 0)
		    {
		      tabPosition = movePiece(tabPiece, largeurGrille, hauteurGrille, Grille, -1, tabPosition);
		      actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                      futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
		    }
                    break;
                case SDLK_RIGHT:
		    if(*perdu == 0 && *jeuEnCours == 1 && *jeuEnPause == 0)
		    {
		      tabPosition = movePiece(tabPiece, largeurGrille, hauteurGrille, Grille, 1, tabPosition);
		      actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                      futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
		    }
                    break;
                case SDLK_UP:
		    if(*perdu == 0 && *jeuEnCours == 1 && *jeuEnPause == 0)
		    {
		      tabPosition = rotation(tabPiece, largeurGrille, hauteurGrille, Grille, tabPosition);
		      actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                      futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
		    }
                    break;
                case SDLK_DOWN:
		    if(*perdu == 0 && *jeuEnCours == 1 && *jeuEnPause == 0)
		    {
		      tabPosition = movePiece_down(tabPiece, largeurGrille, hauteurGrille, Grille, tabPosition, nbPiece(), screen, nbLigne, numLevel, score, perdu);
		      actu(screen, largeurGrille, hauteurGrille, Grille, *jeuEnCours, *jeuEnPause, *hoverQuit, *hoverPause, *hoverPlay, *nbLigne, *numLevel, *score, *perdu);
                      futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
		    }
                    break;
                default:
                    break;
            }
            break;
    }
    return tabPosition;
}

int main( int argc, char* argv[] )
{ 
    SDL_Surface *screen;
    int *tabPosition;
    int tempsActuel = 0, tempsPrecedent = 0;
    int jeuEnCours = 0, jeuEnPause = 0, hoverQuit = 0, hoverPause = 0, hoverPlay = 0, perdu = 0;
    int temp = 1000;
    int nbLigne, numLevel, score;
    nbLigne = 0;
    numLevel = 1;
    score = 0;

    SDL_Event event;
    int gameover = 0;
    
    screen = createWindow();
    
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024);
    Mix_Music *musique;
    musique = Mix_LoadMUS("Tetris Official Theme song.mp3");
    Mix_PlayMusic(musique, -1);
    
    //draw the background
    screen = drawBackground(screen);
    
    srand(time(NULL));
    int numPiece = rand() % nbPiece();
    int numCouleur = rand() % NB_COULEUR;
    Piece *tabPiece = enregistrementPiece();
    
    Carre **Grille = initialisationGrille(hauteurGrille(), largeurGrille());
    
    tabPosition = afficherPiece(tabPiece, numPiece, numCouleur, largeurGrille(), hauteurGrille(), Grille, nbPiece(), screen, &perdu);
    
    actu(screen, largeurGrille(), hauteurGrille(), Grille, jeuEnCours, jeuEnPause, hoverQuit, hoverPause, hoverPlay, nbLigne, numLevel, score, perdu);
    futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
    
    //main loop: check events and re-draw the window until the end
    while (!gameover)
    {
        //look for an event; possibly update the position and the shape of the sprite
        
	tempsActuel = SDL_GetTicks();
	if (tempsActuel - tempsPrecedent > (temp - (numLevel * 100)) && perdu == 0)
	{
            if(jeuEnCours == 1 && jeuEnPause == 0)
            {
                tabPosition = movePiece_down(tabPiece, largeurGrille(), hauteurGrille(), Grille, tabPosition, nbPiece(), screen, &nbLigne, &numLevel, &score, &perdu);
                actu(screen, largeurGrille(), hauteurGrille(), Grille, jeuEnCours, jeuEnPause, hoverQuit, hoverPause, hoverPlay, nbLigne, numLevel, score, perdu);
                futurePiece(tabPiece, tabPosition[4], tabPosition[6], screen);
            }
	    
	    tempsPrecedent = tempsActuel;
	}
	
	if (SDL_PollEvent(&event)) 
        {
            tabPosition = HandleEvent(event, &gameover, tabPiece, largeurGrille(), hauteurGrille(), Grille, tabPosition, screen, &jeuEnCours, &jeuEnPause, &hoverQuit, &hoverPause, &hoverPlay, &nbLigne, &numLevel, &score, &perdu);
        }
	
	//update the screen
	SDL_UpdateRect(screen, 0, 0, 0, 0);
    }

    //clean up
    free(Grille);
    free(tabPosition);
    free(tabPiece);
    Mix_FreeMusic(musique);
    Mix_CloseAudio();
    SDL_FreeSurface(screen);
    SDL_Quit();

    return 0;
}
