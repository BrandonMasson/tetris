SRC=tetris.c fichier.c piece.c graphique.c
BIN=tetris

$(BIN): $(SRC)
	gcc -Wall -g -std=c99 $(SRC) `sdl-config --cflags --libs` -o $(BIN) -lSDL_mixer

clean:
	rm -f $(BIN)
