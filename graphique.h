#include <SDL/SDL.h>

/* Size of the window */
#define SCREEN_WIDTH 479
#define SCREEN_HEIGHT 460

#define SPRITE_SIZE 32
#define BORD_SIZE 6

#define NB_COULEUR 6

#define FICHIER_FOND "images/fond.bmp"

#define FICHIER_LABEL_LEVEL "images/label_level.bmp"
#define FICHIER_LABEL_LINES "images/label_lines.bmp"
#define FICHIER_LABEL_SCORE "images/label_score.bmp"
#define FICHIER_LABEL_QUIT "images/label_quit.bmp"
#define FICHIER_LABEL_QUIT_HOVER "images/label_quit_hover.bmp"
#define FICHIER_LABEL_PAUSE "images/label_pause.bmp"
#define FICHIER_LABEL_PAUSE_HOVER "images/label_pause_hover.bmp"
#define FICHIER_LABEL_PLAY "images/label_play.bmp"
#define FICHIER_LABEL_TETRIS "images/label_tetris.bmp"
#define FICHIER_LABEL_0 "images/label_0.bmp"
#define FICHIER_LABEL_1 "images/label_1.bmp"
#define FICHIER_LABEL_2 "images/label_2.bmp"
#define FICHIER_LABEL_3 "images/label_3.bmp"
#define FICHIER_LABEL_4 "images/label_4.bmp"
#define FICHIER_LABEL_5 "images/label_5.bmp"
#define FICHIER_LABEL_6 "images/label_6.bmp"
#define FICHIER_LABEL_7 "images/label_7.bmp"
#define FICHIER_LABEL_8 "images/label_8.bmp"
#define FICHIER_LABEL_9 "images/label_9.bmp"

#define FICHIER_CARRE_BLEU_CLAIR "images/carre_bleu_clair.bmp"
#define FICHIER_CARRE_BLEU_FONCE "images/carre_bleu_fonce.bmp"
#define FICHIER_CARRE_JAUNE "images/carre_jaune.bmp"
#define FICHIER_CARRE_ORANGE "images/carre_orange.bmp"
#define FICHIER_CARRE_ROUGE "images/carre_rouge.bmp"
#define FICHIER_CARRE_VERT "images/carre_vert.bmp"

#define FICHIER_GRILLE_HAUT_GAUCHE "images/grille_coin_haut_gauche.bmp"
#define FICHIER_GRILLE_HAUT_DROIT "images/grille_coin_haut_droit.bmp"
#define FICHIER_GRILLE_BAS_GAUCHE "images/grille_coin_bas_gauche.bmp"
#define FICHIER_GRILLE_BAS_DROIT "images/grille_coin_bas_droit.bmp"
#define FICHIER_GRILLE_HORIZONTAL "images/grille_horizontal.bmp"
#define FICHIER_GRILLE_VERTICAL "images/grille_vertical.bmp"
#define FICHIER_GRILLE_MILIEU "images/grille_milieu.bmp"

SDL_Surface *createWindow();

int realSpriteSize();

SDL_Surface *drawBackground(SDL_Surface *screen);

SDL_Surface *createSprite(char nomFichier[]);

SDL_Surface *createSpriteWH(char nomFichier[], int w, int h);

void BlitIntSprite(char *nombre, SDL_Surface *screen, SDL_Rect gridImage);

void afficherCarre(int x, int y, SDL_Surface *screen, SDL_Surface *sprite);

char *couleurCarre(int numCouleur);