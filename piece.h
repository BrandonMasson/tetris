#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>

#define POINT 100

typedef struct Element Element;
struct Element
{
  int nombre;
  Element *suivant;
};

typedef struct Piece Piece;
struct Piece
{
  Element *premier;
  int nbElement;
};

typedef struct Carre Carre;
struct Carre
{
  int estPlein;
  int numCouleur;
};

//Initialise une liste de Piece
Piece *initialisationPiece();

//Insert une element dans la liste
void insertionPiece(Piece *liste, int nvNombre);

//Affiche toute les piece contenue dans le tableau

int *afficherPiece(Piece *tabPiece, int numPiece, int numCouleur, int largeurGrille, int hauteurGrille, Carre **Grille, int nbPiece, SDL_Surface *screen, int *perdu);

Carre **initialisationGrille(int hauteurGrille, int largeurGrille);

int largeurPiece(int numPiece, Piece *tabPiece);

int hauteurPiece(int numPiece, Piece *tabPiece);

int *movePiece(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int x, int tab[]);

int *movePiece_down(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[], int nbPiece, SDL_Surface *screen, int *nbLigne, int *numLevel, int *score, int *perdu);

int peut_bouger(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int x, int tab[]);

int peut_descendre(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[]);

int apparition_possible(Piece *tabPiece, int numPiece, int largeurGrille, int hauteurGrille, Carre **Grille);

int verifie(int largeurGrille, int hauteurGrille, Carre **Grille);

int faitPartie(Piece *tabPiece, Carre **Grille, int tab[], int x, int y);

int *rotation(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[]);

int rotation_possible(Piece *tabPiece, Carre **Grille, int tab[]);

void futurePiece(Piece *tabPiece, int numPiece, int numCouleur, SDL_Surface *screen);

void actu(SDL_Surface *screen, int largeur_Grille, int hauteur_Grille, Carre **Grille, int jeuEnCours, int jeuEnPause, int hoverQuit, int hoverPause, int hoverPlay, int nbLigne, int numLevel, int Score, int perdu);

int score(int nbligne);

int calculLevel(int nbligne);

int peutTourner(Piece *tabPiece, int largeurGrille, int hauteurGrille, Carre **Grille, int tab[]);