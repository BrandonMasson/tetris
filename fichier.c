#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fichier.h"

int nbPiece()
{
    FILE* fichier;
    int comptPiece = 0;
    int caractereActuel = 0;
    
    fichier = fopen(NOM_FICHIER, "r");
    
    if (fichier != NULL)
    {
      do
      {
	caractereActuel = fgetc(fichier);
	if(strchr(CHAR_FIN, caractereActuel) != NULL)
	{
	  comptPiece += 1;
	} 
      } while (caractereActuel != EOF);
    }
    
    fclose(fichier);
    return comptPiece;
}

int largeurGrille()
{
    FILE* fichier;
    int comptLargeur = 0;
    int caractereActuel = 0;
    
    fichier = fopen(NOM_FICHIER, "r");
    
    if (fichier != NULL)
    {
      do
      {
	caractereActuel = fgetc(fichier);
	if(strchr(CHAR_GRILLE, caractereActuel) != NULL)
	{
	  comptLargeur += 1;
	} 
      } while (strchr(CHAR_GRILLE_RETOUR, caractereActuel) == NULL);
    }
    
    fclose(fichier);
    return comptLargeur;
}

int hauteurGrille()
{
    FILE* fichier;
    int comptHauteur = 0;
    int caractereActuel = 0;
    
    fichier = fopen(NOM_FICHIER, "r");
    
    if (fichier != NULL)
    {
      do
      {
	caractereActuel = fgetc(fichier);
	if(strchr(CHAR_GRILLE_RETOUR, caractereActuel) != NULL)
	{
	  comptHauteur += 1;
	} 
      } while (strchr(CHAR_GRILLE_FIN, caractereActuel) == NULL);
    }
    
    fclose(fichier);
    return comptHauteur + 1;
}

Piece *enregistrementPiece()
{
    int caractereActuel = 0;
    int i = 0;
    Piece *maListe = initialisationPiece();
    Piece *tabPiece = NULL;
    tabPiece = malloc(nbPiece() * sizeof(Piece));
    
    FILE* fichier = fopen(NOM_FICHIER, "r");
    
    if (fichier != NULL)
    {
      do
      {
	caractereActuel = fgetc(fichier); // On lit le caract�re
	if(strchr("#", caractereActuel) != NULL)
	{
	  /* choose image according to direction and animation flip: */
	  /*SDL_BlitSurface(sprite, &spriteImage, screen, &pos);
	  pos.x = pos.x + SPRITE_SIZE;*/
	  insertionPiece(maListe, 1);
	}
	else
	{
	  if(strchr("R", caractereActuel) != NULL)
	  {
	    /*pos.x = 0;
	    pos.y = pos.y + SPRITE_SIZE ;*/
	    insertionPiece(maListe, 2);
	  }
	  else
	  {
	    if(strchr("_", caractereActuel) != NULL)
	    {
	      /*SDL_BlitSurface(vide, &videImage, screen, &pos);
	      pos.x = pos.x + SPRITE_SIZE;*/
	      insertionPiece(maListe, 0);
	    }
	    else
	    {
	      if(strchr("F", caractereActuel) != NULL)
	      {
		tabPiece[i] = *maListe;
		free(maListe);
		maListe = initialisationPiece();
		i += 1;
	      } 
	    }
	  }
	}
	
      } while (caractereActuel != EOF);
    }
    //printf("%d", countListePiece(taListe));
    fclose(fichier);
    return tabPiece;
}
