#include "graphique.h"
#include "fichier.h"

SDL_Surface *createWindow()
{
    SDL_Init(SDL_INIT_VIDEO);
    
  /* set the title bar */
    SDL_WM_SetCaption("Tetris", "Tetris");

    /* create window */
    putenv("SDL_VIDEO_WINDOW_POS=center");
    SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT, 0, 0);
    
    return screen;
}

int realSpriteSize()
{
    int size = SPRITE_SIZE;
    
    while((size * hauteurGrille()) > SCREEN_HEIGHT || (size * largeurGrille()) > (SCREEN_WIDTH / 2))
    {
        size = size - 2;
    }
    
    return size;
}

SDL_Surface *drawBackground(SDL_Surface *screen)
{
    SDL_Rect position;
    position.x = 0;
    position.y = 0;
    
    SDL_Surface *fond = createSprite(FICHIER_FOND);
    fond->w = SPRITE_SIZE;
    fond->h = SPRITE_SIZE;
    
    for(int i = 0; i <= (SCREEN_HEIGHT / SPRITE_SIZE); i++)
    {
        position.y = SPRITE_SIZE * i;
        
        for(int j = 0; j <= (SCREEN_WIDTH / SPRITE_SIZE); j++)
        {
            position.x = SPRITE_SIZE * j;
            SDL_BlitSurface(fond, NULL, screen, &position);
        }
    }
    
    return screen;
}

SDL_Surface *createSprite(char *nomFichier)
{
    SDL_Surface *temp, *sprite;
    int size = realSpriteSize();
    
    temp = SDL_LoadBMP(nomFichier);
    sprite = SDL_DisplayFormat(temp);
    sprite->w = size;
    sprite->h = size;
    SDL_FreeSurface(temp);
    
    return sprite;
}

SDL_Surface *createSpriteWH(char nomFichier[], int w, int h)
{
    SDL_Surface *temp, *sprite;
    
    temp = SDL_LoadBMP(nomFichier);
    sprite = SDL_DisplayFormat(temp);
    sprite->w = w;
    sprite->h = h;
    
    SDL_FreeSurface(temp);
    
    return sprite;
}

void BlitIntSprite(char *nombre, SDL_Surface *screen, SDL_Rect gridImage)
{
    char *nomFichier = NULL;
    gridImage.h = 32;
    
    for(int i = strlen(nombre); i >= 0; i--)
    {
        switch(nombre[i])
        {
            case '0' :
                nomFichier = FICHIER_LABEL_0;
                gridImage.w = 19;
                break;
                
            case '1' :
                nomFichier = FICHIER_LABEL_1;
                gridImage.w = 10;
                break;
                
            case '2' :
                nomFichier = FICHIER_LABEL_2;
                gridImage.w = 19;
                break;
                
            case '3' :
                nomFichier = FICHIER_LABEL_3;
                gridImage.w = 20;
                break;
                
            case '4' :
                nomFichier = FICHIER_LABEL_4;
                gridImage.w = 20;
                break;
                
            case '5' :
                nomFichier = FICHIER_LABEL_5;
                gridImage.w = 20;
                break;
                
            case '6' :
                nomFichier = FICHIER_LABEL_6;
                gridImage.w = 20;
                break;
                
            case '7' :
                nomFichier = FICHIER_LABEL_7;
                gridImage.w = 19;
                break;
                
            case '8' :
                nomFichier = FICHIER_LABEL_8;
                gridImage.w = 18;
                break;
                
            case '9' :
                nomFichier = FICHIER_LABEL_9;
                gridImage.w = 18;
                break;
        }
        
        if(nomFichier != NULL)
        {
            SDL_BlitSurface(createSpriteWH(nomFichier, gridImage.w, gridImage.h), NULL, screen, &gridImage);
            gridImage.x -= gridImage.w + 5;
        }
        
        nomFichier = NULL;
    }
}

void afficherCarre(int x, int y, SDL_Surface *screen, SDL_Surface *sprite)
{
    int sprite_size = realSpriteSize();
    
    SDL_Rect position;
    position.x = x;
    position.y = y;
    position.w = sprite_size;
    position.h = sprite_size;
    
    SDL_BlitSurface(sprite, NULL, screen, &position);
}

char *couleurCarre(int numCouleur)
{
    char *res = NULL;

    switch(numCouleur)
    {
        case 0:
            res = FICHIER_CARRE_BLEU_CLAIR;
            break;
        case 1:
            res = FICHIER_CARRE_BLEU_FONCE;
            break;
        case 2:
            res = FICHIER_CARRE_JAUNE;
            break;
        case 3:
            res = FICHIER_CARRE_ORANGE;
            break;
        case 4:
            res = FICHIER_CARRE_ROUGE;
            break;
        case 5:
            res = FICHIER_CARRE_VERT;
            break;
    }

    return res;
}

